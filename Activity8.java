package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity8 {
	WebDriver driver;

	@BeforeSuite
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	}

	@Test(priority = 1)
	public void clkPostJobLink() {
		WebElement enterUserNameOrEmail = driver.findElement(By.id("user_login"));
		enterUserNameOrEmail.sendKeys("test011");
		
		WebElement enterpassword = driver.findElement(By.id("user_pass"));
		enterpassword.sendKeys("abcde");
		
		WebElement clkLogin = driver.findElement(By.id("wp-submit"));
		clkLogin.click();
	}

	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}
}
