package projassess_activity;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Activity2 {

	WebDriver driver;
	
@Test(priority=1)
	public void openBrowser() {
	System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");

		driver = new FirefoxDriver();
		
		driver.get("https://alchemy.hguy.co/jobs");
	
		String actualText = driver.findElement(By.xpath("//h1[@class='entry-title']")).getText();
		String expectedText = "Welcome to Alchemy Jobs";
		
		if(actualText.equalsIgnoreCase(expectedText)) {
			System.out.println("The Heading is exactly Matching");
		} else {
			System.out.println("The Heading is not Matching");
		}
	}
@Test(priority=2)
	public void closeBrowser() {
		driver.close();
}
}
