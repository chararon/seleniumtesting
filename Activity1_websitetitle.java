package projassess_activity;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Activity1_websitetitle {
	WebDriver driver;
	
	//Open a browser
	@Test(priority=1)
		
	public void openBrowser() {
		//Open a browser
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver=new FirefoxDriver();
		
		driver.get("https://alchemy.hguy.co/jobs");
			//Get the title of the website	
			
		String actualTitle=driver.getTitle();
		String expectedTitle="Alchemy Jobs � Job Board Application";
		//actual Title match with expected title
			
	    if(actualTitle.equalsIgnoreCase(expectedTitle)) {
	        System.out.println("Title is Matching");
	    } else {
	        System.out.println("The title is not matching");
	    } 	
	}
	//Close the browser
	@Test(priority=2)
	  public void closeBrowser() {
		  driver.close();
}
}