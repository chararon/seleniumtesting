package projassess_activity;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Activity5 {
	WebDriver driver;
	
	@Test(priority=1)
	//Open a browser
		public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
			driver = new FirefoxDriver();
	//Navigate to �https://alchemy.hguy.co/jobs�
			driver.get("https://alchemy.hguy.co/jobs");
	//Find and Select the menu item that says �Jobs� and click it	
			driver.findElement(By.linkText("Jobs")).click();
			String navigatedTitle = driver.getTitle();
			String expectedTitle ="Jobs � Alchemy Jobs";
			
			if(navigatedTitle.equalsIgnoreCase(expectedTitle)) {
				System.out.println("User Navigated to Correct Page");
			}else {
				System.out.println("User Landing in Wrong Page");	
			}
			
			
		}
	@Test(priority=2)
		public void closeBrowser() {
			driver.close();
	}
	}

