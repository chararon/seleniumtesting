package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity6 {
	WebDriver driver;

	@BeforeSuite
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs");
	}

	@Test(priority = 1)
	public void clkJobsLink() {
		driver.findElement(By.linkText("Jobs")).click();
	}

	@Test(priority = 2)
	public void searchJobs() {
		WebElement searchJobs = driver.findElement(By.id("search_keywords"));
		searchJobs.sendKeys("Java");
		WebElement clkSearchJobs = driver.findElement(By.xpath("//input[@value='Search Jobs']"));
		clkSearchJobs.click();

	}

	@Test(priority = 3)
	public void clkJobOpen() {
		driver.findElement(By.xpath("//h3[contains(text(),'Java')]//following::div[1] ")).click();
	}

	@Test(priority = 4)
	public void clkApplyButton() {
		WebElement clkApplyJob = driver.findElement(By.xpath("//input[@type='button']"));
		clkApplyJob.click();
	}

	@Test(priority = 5)
	public void printEmail() {
		String emailid = driver.findElement(By.xpath("//a[@class='job_application_email']")).getText();
		System.out.println("The Email ID is = " +emailid);
	}

	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}
}
