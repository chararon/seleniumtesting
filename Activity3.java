package projassess_activity;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Activity3 {
	WebDriver driver;
	
	@Test(priority=1)
		public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
			driver = new FirefoxDriver();
			
			driver.get("https://alchemy.hguy.co/jobs");
			
			String urlHeader =  driver.getCurrentUrl();
			System.out.println("The URL of the header image is: "+urlHeader);
			
		}
	@Test(priority=2)
		public void closeBrowser() {
			driver.close();
	}
	}

