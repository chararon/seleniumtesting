package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity11_CEA {
	WebDriver driver;
	
	@BeforeSuite
	public void openBrowser() {
		System.getProperty("webdriver.gecko.driver", "C:\\Reskill Training\\geckodriver.exe");
		driver= new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs");
	}
	@Test (priority=1)
	public void SearchInput() {
		WebElement clkjobs=driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[1]/a"));
		clkjobs.click();
		
		WebElement searchJobs=driver.findElement(By.id("search_keywords"));
		searchJobs.sendKeys("Java");
		
		WebElement clkSearchJobs=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/form/div[1]/div[4]/input"));
		clkSearchJobs.click();
		boolean unselectfreelance=driver.findElement(By.id("job_type_freelance")).isSelected();
		if (unselectfreelance==true)
			driver.findElement(By.id("job_type_freelance")).click();
		
		boolean unselectinternship=driver.findElement(By.id("job_type_internship")).isSelected();
		if (unselectinternship==true)
			driver.findElement(By.id("job_type_internship")).click();
		
		boolean unselectPartTime=driver.findElement(By.id("job_type_part-time")).isSelected();
		if (unselectPartTime==true)
			driver.findElement(By.id("job_type_part-time")).click();
		
		boolean unselectTemporary=driver.findElement(By.id("job_type_temporary")).isSelected();
		if (unselectTemporary==true)
			driver.findElement(By.id("job_type_temporary")).click();
		}
	@Test (priority=2)
	public void joblisting() {
		WebElement joblisting=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/ul/li/a/div[1]/h3"));
		joblisting.click();
				
	}
	@Test (priority=3)
	public void printjobdetails() {
		String printjob=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/header/div[2]/h1")).getText();
		System.out.println("Title of the job listing:" +printjob);
			}
	@Test(priority=4)
	public void clkApplyforjob() {
		WebElement btnclk=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/div/div[3]/input"));
		btnclk.click();
	}
	
	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}

}
