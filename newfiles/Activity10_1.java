package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity10_1 {
	WebDriver driver;

	@BeforeSuite
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	}

	@Test(priority = 1)
	public void adminLogin() {
		WebElement enterUserNameOrEmail = driver.findElement(By.id("user_login"));
		enterUserNameOrEmail.sendKeys("root");
		WebElement enterpassword = driver.findElement(By.id("user_pass"));
		enterpassword.sendKeys("pa$$w0rd");
		WebElement clkLogin = driver.findElement(By.id("wp-submit"));
		clkLogin.click();
	}
	@Test(priority = 2)
	public void clkUsers() throws Exception {
	
		driver.findElement(By.xpath("//div[contains(text(),'Users')]")).click();
		driver.findElement(By.cssSelector(".page-title-action")).click();

	}
	@Test(priority= 3)
	public void enterDetails() throws Exception {
		driver.findElement(By.id("user_login")).sendKeys("caron");
		driver.findElement(By.id("email")).sendKeys("test1@gmail.com");
		driver.findElement(By.id("first_name")).sendKeys("Charles");
		driver.findElement(By.id("last_name")).sendKeys("Aron");
		driver.findElement(By.xpath("//button[contains(text(),'Show password')]")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("pass1")).clear();
		driver.findElement(By.id("pass1")).sendKeys("test1234");
		driver.findElement(By.id("createusersub")).click();

		
	}
	@Test(priority = 4)
	public void verifyAddUserListing() {
	//	driver.findElement(By.xpath("//div[@class='block-editor-editor-skeleton__header']/div/a")).click();

		WebElement verifyusr = driver.findElement(By.xpath("//tr[@id='user-399']/td[1])"));
		String jobcrat = verifyusr.getText();
		if (jobcrat.equalsIgnoreCase("user1")) {
			System.out.println("New user created in Add user List");
		} else {
			System.out.println("Not created");
		}
	}
	@AfterSuite
	public void closeBrowser() {
		driver.close();
}
}
