package projassess_activity;

import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class Activity12_CEA {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	
	@BeforeClass
	public void login() {
		driver = new FirefoxDriver();
		wait=new WebDriverWait(driver, 40);
		action =new Actions(driver);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
		
	}
	
	@Test(priority=0)
	public void websiteBackEndLogin() {
		Reporter.log("Logging into backend website");
		driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-submit")));
		driver.findElement(By.id("wp-submit")).click();
		driver.manage().window().maximize();
		
	}
	@Test(priority=1)
	public void createUser() {
		//WebElement user = driver.findElement(By.xpath("//input[contains(@type,'submit')]"));
		WebElement user = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[11]/a/div[2]"));
		action.moveToElement(user).perform();
		Reporter.log("Navigate to Add New User and click");
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Add New")));
		driver.findElement(By.linkText("Add New")).click();
	
	}
	@Test(priority=2)
	public void createUserCSV()	throws CsvException, IOException {
			
		CSVReader reader = new CSVReader(new FileReader("C:\\Users\\CHARLESARON\\eclipse-workspace\\Java-Sample1\\Selenium_Activity\\src\\projassess_activity\\Activity12_testdata.csv.txt"));
		String[] cell; 
		while ((cell=reader.readNext()) !=null) {
			for(int i=0;i<1;i++) {
				String Username = cell[i];
				String Email = cell[i+1];
				String FirstName  = cell[i+2];
				String LastName  = cell[i+3];
				String Website  = cell[i+4];
				String passwrd  = cell[i+5];
				String role  = cell[i+6];
				
			driver.findElement(By.id("user_login")).sendKeys(Username);
			driver.findElement(By.id("email")).sendKeys(Email);
			driver.findElement(By.id("first_name")).sendKeys(FirstName);
			driver.findElement(By.id("last_name")).sendKeys(LastName);
            driver.findElement(By.id("url")).sendKeys(Website);
            
          driver.findElement(By.xpath("//button[contains(@class,'wp-generate-pw')]")).click();
      	  WebElement pwdButton = driver.findElement(By.id("pass1"));
      	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1")));
      	  pwdButton.clear();
				
      	  ((JavascriptExecutor) driver).executeScript("arguments[0].value='" +passwrd + "';", pwdButton);
      	  driver.findElement(By.id("role")).sendKeys(role);
      	  
      	Reporter.log("Clicking Add User button");
      	driver.findElement(By.cssSelector("input#createusersub")).click();
      	
      	
      	//Verify user created
      	 Reporter.log("Verify user added successfully");
   	  WebElement addSuccess = driver.findElement(By.cssSelector("div#message"));
   	  System.out.println(addSuccess.getText());
   	  String message = "New user created. Edit user\nDismiss this notice.";
   	  Assert.assertEquals(message, addSuccess.getText());
			}
		}
			
	}
		
		
@AfterClass
public void afterClass() {
	  driver.close();
}

		}
	


