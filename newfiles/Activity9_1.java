package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity9_1 {
	WebDriver driver;

	@BeforeSuite
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	}

	@Test(priority = 1)
	public void adminLogin() {
		WebElement enterUserNameOrEmail = driver.findElement(By.id("user_login"));
		enterUserNameOrEmail.sendKeys("root");
		WebElement enterpassword = driver.findElement(By.id("user_pass"));
		enterpassword.sendKeys("pa$$w0rd");
		WebElement clkLogin = driver.findElement(By.id("wp-submit"));
		clkLogin.click();
	}

	@Test(priority = 2)
	public void clkJobListingLnk() throws Exception {
		driver.findElement(By.id("menu-posts-job_listing")).click();
		driver.findElement(By.cssSelector(".page-title-action")).click();
		driver.findElement(By.cssSelector(".components-modal__header button:nth-child(2) svg")).click();
	}

	@Test(priority = 3)
	public void enterDetails() throws Exception {
		driver.findElement(By.id("post-title-0")).sendKeys("Automation");
		driver.findElement(By.name("_job_location")).sendKeys("Bengaluru");
		driver.findElement(By.name("_company_name")).sendKeys("IBM");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[contains(text(),'Publish�')]")).click();
	}

	@Test(priority = 4)
	public void verifyJobListing() {
		driver.findElement(By.xpath("//div[@class='block-editor-editor-skeleton__header']/div/a")).click();

		WebElement verifytxt = driver.findElement(By.xpath("//tbody[@id='the-list']/tr/td[1]"));
		String jobcrat = verifytxt.getText();
		if (jobcrat.equalsIgnoreCase("Automation")) {
			System.out.println("Created Job is Listing in JOb Listing");
		} else {
			System.out.println("Job Not listing");
		}
	}

	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}

}
