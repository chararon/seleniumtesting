package projassess_activity;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;

public class Activity13_CEA {
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeClass
	  public void browserOpen() {
		  driver = new FirefoxDriver();
		  wait = new WebDriverWait(driver,30);
		  driver.get("https://alchemy.hguy.co/jobs/");
	  }
	@Test(priority=0)
	public void jobPostingCSV() throws FileNotFoundException, Exception {
		Reporter.log("Navigating to Post a Job");
		  driver.findElement(By.linkText("Post a Job")).click();
		  driver.findElement(By.cssSelector("a.button")).click();	
		  
		  driver.findElement(By.id("user_login")).sendKeys("root");
		  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		  WebElement loginSubmit = driver.findElement(By.id("wp-submit"));
		  wait.until(ExpectedConditions.visibilityOf(loginSubmit));
		  loginSubmit.click();
		  
		  
		  
		  CSVReader reader =new CSVReader (new FileReader("C:\\Users\\CHARLESARON\\eclipse-workspace\\Java-Sample1\\Selenium_Activity\\src\\projassess_activity\\activity13.csv"));
		  String[] cell;
		  while ((cell = reader.readNext()) != null) {
			for (int i=0;i<1;i++) {
				String jobTitle = cell[i];
				  String location = cell[i + 1];
				  String jobType = cell[i + 2];
				  String desc = cell[i + 3];
				  String company = cell[i + 4];
				  String website = cell[i + 5];
				  String tagLine = cell[i + 6];
				  String twitter = cell[i + 7];
				  
				  driver.findElement(By.id("job_title")).sendKeys(jobTitle);
				  driver.findElement(By.id("job_location")).sendKeys(location);
				  driver.findElement(By.id("job_type")).sendKeys(jobType);
				  driver.switchTo().frame("job_description_ifr");
				  driver.findElement(By.id("tinymce")).sendKeys(desc);
				  
				driver.switchTo().defaultContent();
				
				WebElement CompName = driver.findElement(By.id("company_name"));
				CompName.clear();
				CompName.sendKeys(company);
				
				WebElement WebsiteName=driver.findElement(By.id("company_website"));
				WebsiteName.clear();
				WebsiteName.sendKeys(website);
				
				WebElement tagName = driver.findElement(By.id("company_tagline"));
				tagName.clear();
				tagName.sendKeys(tagLine);
				
				driver.findElement(By.id("company_video")).clear();
				
				WebElement twitterName = driver.findElement(By.id("company_twitter"));
				twitterName.clear();
				twitterName.sendKeys(twitter);
				
				
				Reporter.log("Click on preview button");
				  WebElement preview = driver.findElement(By.cssSelector("input.button"));
				  wait.until(ExpectedConditions.elementToBeClickable(preview));
				  preview.click();
				  
				//Opening Preview page
				  wait.until(ExpectedConditions.elementToBeClickable(By.id("job_preview_submit_button")));
				  Reporter.log("Click on Submit Listing");
				  driver.findElement(By.id("job_preview_submit_button")).click();  
				  Reporter.log("Job posted successfully");
				  WebElement postSuccess = driver.findElement(By.xpath("//div[@class='entry-content clear']"));
				  System.out.println(postSuccess.getText());
						
			}
				
			}
			
		  }
		  @AfterClass
		  public void browserClose() {
			  driver.close();
	}

}
