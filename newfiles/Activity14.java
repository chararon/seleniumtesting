package projassess_activity;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Activity14 {
	WebDriver driver;
	WebDriverWait wait;
	Actions actions;
	
	@DataProvider(name="Authentication")
	public static Object[][] credentials() {
        return new Object[][] { { "root", "pa$$w0rd" }};
    }
	
	@DataProvider(name="createUser")
	public static Object[] [] addUser(){
		return new Object[][] {{"caron", "test1@gmail.com", "Charl1", "Aron","IBMCorp.com","@Chal12kjU","Contributor"}};
		
	}
	
  @Test(dataProvider = "Authentication")
  public void login(String username, String password) {
	  //Find username and password fields
      WebElement usernameField = driver.findElement(By.id("user_login"));
      WebElement passwordField = driver.findElement(By.id("user_pass"));

      //Enter values
      usernameField.sendKeys(username);
      passwordField.sendKeys(password);	
      
      //Login button click
      wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-submit")));
	  driver.findElement(By.id("wp-submit")).click();
	  Reporter.log("login success");
	  driver.manage().window().maximize();
  }
  @Test (dependsOnMethods= {"login"})
  public void addUserClick() {
	  
	  WebElement user = driver.findElement(By.xpath("//div[@class='wp-menu-image dashicons-before dashicons-admin-users']"));
	  actions.moveToElement(user).perform();
	  
	  Reporter.log("Clicking on Add New User from User menu");
	  wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Add New")));
	  driver.findElement(By.linkText("Add New")).click();
	  
  }
  @Test(dependsOnMethods= {"addUserClick"}, dataProvider ="createUser")
  public void addUser(String name, String email, String firstname, String lastname, String website,String pwd, String role) {
	  
	  //Form input
	  Reporter.log("Form input");
	  driver.findElement(By.name("user_login")).sendKeys(name);
	  driver.findElement(By.name("email")).sendKeys(email);
	  driver.findElement(By.id("first_name")).sendKeys(firstname);
	  driver.findElement(By.name("last_name")).sendKeys(lastname);
	  driver.findElement(By.id("url")).sendKeys(website);
	 
	  //password button
	  driver.findElement(By.xpath("//button[contains(@class,'wp-generate-pw')]")).click();
	  WebElement pwdButton = driver.findElement(By.id("pass1"));
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1")));
	  pwdButton.clear();
	  ((JavascriptExecutor) driver).executeScript("arguments[0].value='" +pwd + "';", pwdButton);
	  
	  //Role input
	  driver.findElement(By.id("role")).sendKeys(role);
	  
	  Reporter.log("Clicking Add User button");
	  driver.findElement(By.cssSelector("input#createusersub")).click();
	  
	  //Verify user is created
	  Reporter.log("Verify user added successfully");
	  WebElement addSuccess = driver.findElement(By.cssSelector("div#message"));
	  System.out.println(addSuccess.getText());
	  String message = "New user created. Edit user\nDismiss this notice.";
	  Assert.assertEquals(message, addSuccess.getText());
  }
  @BeforeTest
  public void beforeTest() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,40);
	  actions = new Actions(driver);
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin");
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}


}
