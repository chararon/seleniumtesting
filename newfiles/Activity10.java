package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity10 {
	WebDriver driver;
	static WebElement usermenu, addnew;
	@BeforeSuite
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	}

	@Test(priority = 1)
	public void clkPostJobLink() {
		WebElement enterUserNameOrEmail = driver.findElement(By.id("user_login"));
		enterUserNameOrEmail.sendKeys("root");
		
		WebElement enterpassword = driver.findElement(By.id("user_pass"));
		enterpassword.sendKeys("pa$$w0rd");
		
		WebElement clkLogin = driver.findElement(By.id("wp-submit"));
		clkLogin.click();
	}
	@Test(priority = 2)
	public void mousHover() throws Exception {
		// usermenu = driver.findElement(By.xpath("//*[@id=\\\"menu-users\\\"]/a/div[2]"));
		//usermenu = driver.findElement(By.xpath("//*[@class=\"wp-has-submenu wp-not-current-submenu menu-top menu-icon-users"));
		driver.findElement(By.xpath("//div[@class='wp-menu-image dashicons-before dashicons-admin-users']")).click();
		
		// wp-menu-name
		addnew = driver.findElement(By.xpath("//*[@id=\"menu-users\"]/a/div[2]/ul/li"));
		Actions action = new Actions(driver);
		action.moveToElement(usermenu).perform();
		action.moveToElement(addnew).perform();
		action.moveToElement(addnew).click().perform();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@class=\"wp-has-submenu wp-not-current-submenu menu-top menu-icon-users")).click();
		//driver.findElement(By.xpath("//*[@id=\"menu-users\"]/a/div[2]")).click();
		//driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[11]/ul/li[3]/a")).click();
		//driver.findElement(By.xpath("//*[@id=\"wpbody-content\"]/div[3]/a")).click();
	}
	@Test(priority= 3)
	public void enterDetails() throws Exception {
		driver.findElement(By.id("user_login")).sendKeys("caron");
		driver.findElement(By.id("email")).sendKeys("test1@gmail.com");
		driver.findElement(By.id("first_name")).sendKeys("Charles");
		driver.findElement(By.id("last_name")).sendKeys("Aron");
		driver.findElement(By.id("pass1")).sendKeys("Welcome2Alchem!learning");
		driver.findElement(By.id("createusersub")).click();
			
	}
	@Test(priority = 4)
	public void verifyJobListing() {
		driver.findElement(By.xpath("//*[@id=\"username\"]/a/span[2]")).getText();
	}
	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}
}
