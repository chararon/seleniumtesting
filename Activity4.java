package projassess_activity;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Activity4 {
	WebDriver driver;
	
	@Test(priority=1)
		public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
			driver = new FirefoxDriver();
			
			driver.get("https://alchemy.hguy.co/jobs");
			
			String actualsecondHeading =  driver.findElement(By.xpath("//*[@id=\"post-16\"]/div/h2")).getText();
										
			String expectedsecondHeading ="Quia quis non";
			
			if(actualsecondHeading.equalsIgnoreCase(expectedsecondHeading)) {
				System.out.println("The Second Heading is Matching");
			}else {
			System.out.println("Second Heading is Not Matching");
			}
			
		}
	@Test(priority=2)
		public void closeBrowser() {
			driver.close();
	}
	}

