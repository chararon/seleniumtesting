package projassess_activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Activity7 {
	WebDriver driver;

	@BeforeSuite
	public void openBrowser() {
		System.setProperty("webdriver.gecko.driver","C:\\Reskill Training\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://alchemy.hguy.co/jobs");
	}

	@Test(priority = 1)
	public void clkPostJobLink() {
		WebElement clkPostJob = driver.findElement(By.xpath("//a[contains(text(),'Post a Job')]"));
		clkPostJob.click();
	}

	@Test(priority = 2)
	public void enterdeatils() {
		WebElement entrEmail = driver.findElement(By.id("create_account_email"));
		entrEmail.sendKeys("test200@gmail.com");
		WebElement jobTitle = driver.findElement(By.id("job_title"));
		jobTitle.sendKeys("Java1");
		WebElement joblocation = driver.findElement(By.id("job_location"));
		joblocation.sendKeys("Bengaluru");
		WebElement jobType = driver.findElement(By.id("job_type"));
		jobType.sendKeys("Full Time");
		driver.findElement(By.xpath("//iframe[@id='job_description_ifr']")).sendKeys("Java Development");
		WebElement enterapplication = driver.findElement(By.id("application"));
		enterapplication.sendKeys("test200@gmail.com");
		WebElement companyName = driver.findElement(By.id("company_name"));
		companyName.sendKeys("IBM");
		WebElement clkPreview = driver.findElement(By.xpath("//input[@class='button']"));
		clkPreview.click();
		driver.findElement(By.id("job_preview_submit_button")).click();
		String text = driver.findElement(By.xpath("//div[@class='entry-content clear']")).getText();
		System.out.println(text);
	}

	@Test(priority= 3)
	public void verifyjoblisting() {
		driver.findElement(By.linkText("Jobs")).click();
		WebElement verifyjob = driver.findElement(By.id("search_keywords"));
		verifyjob.sendKeys("Java1");
		WebElement srchJob = driver.findElement(By.xpath("//input[@value='Search Jobs']"));
		srchJob.click();
		}

	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}
}
